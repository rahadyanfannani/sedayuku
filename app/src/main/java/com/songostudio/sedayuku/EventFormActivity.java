package com.songostudio.sedayuku;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.songostudio.sedayuku.main.MainActivity;
import com.songostudio.sedayuku.model.Event;

import java.io.ByteArrayOutputStream;

public class EventFormActivity extends AppCompatActivity {

    private EditText edtx_title;
    private EditText edtx_alamat;
    private EditText edtx_deskripsi;
    private Button btn_tambah;
    private Button btn_foto;
    private Bitmap imageBitmap;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private boolean photoState = false;
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            photoState = true;
            imageBitmap = (Bitmap) extras.get("data");

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_form);
        final FirebaseStorage storage = FirebaseStorage.getInstance();

        edtx_title = (EditText) findViewById(R.id.edtx_title);
        edtx_alamat = (EditText) findViewById(R.id.edtx_alamat);
        edtx_deskripsi = (EditText) findViewById(R.id.edtx_deskripsi);
        btn_tambah = (Button) findViewById(R.id.btn_tambah);
        btn_foto = (Button) findViewById(R.id.btn_foto);
        final Intent i = getIntent();
        final String category = i.getStringExtra("category");
        Log.d("CATEGORY",category);

        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference eventsData = database.getReference("events");
                String title = edtx_title.getText().toString();
                String alamat = edtx_alamat.getText().toString();
                String deskripsi = edtx_deskripsi.getText().toString();
                Event newEvent = new Event(title,alamat,deskripsi);
                newEvent.kategori = category;
                DatabaseReference newEventRef = eventsData.push();
                newEventRef.setValue(newEvent);
                if(photoState){
                    StorageReference storageRef = storage.getReference();
                    StorageReference imagesRef = storageRef.child("photos");
                    StorageReference spaceRef = imagesRef.child(newEventRef.getKey());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();

                    UploadTask uploadTask = spaceRef.putBytes(data);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                        }
                    });

                }


            }
        });
        btn_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();

            }
        });



    }
}
