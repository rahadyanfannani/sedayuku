package com.songostudio.sedayuku.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahad on 14/11/2017.
 */

public class Event {
    public String title;
    public String alamat;
    public String deksripsi;
    public String kategori;
    public String id;
    public Map<String, Boolean> likes = new HashMap<>();
    public int likeCount;
    public Event(String title, String alamat, String deskripsi){
        this.title = title;
        this.alamat = alamat;
        this.deksripsi = deskripsi;

    }
    public Event(){

    }
}
