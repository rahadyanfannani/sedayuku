package com.songostudio.sedayuku.model;

/**
 * Created by rahad on 15/11/2017.
 */

public class Category {
    public String title;
    public int icon;

    public Category(String title, int icon){
        this.title = title;
        this.icon = icon;
    }
    public Category(){

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
