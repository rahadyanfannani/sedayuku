package com.songostudio.sedayuku.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.songostudio.sedayuku.R;
import com.songostudio.sedayuku.model.Event;
import org.w3c.dom.Text;

import java.util.ArrayList;


/**
 * Created by rahad on 14/11/2017.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private ArrayList<Event> mDataset;
    private Context mContext;


    // Provide a suitable constructor (depends on the kind of dataset)
    public EventAdapter(Context context,ArrayList<Event> myDataset) {
        mDataset = myDataset;
        mContext = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.events_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(mDataset.get(position).title);
        // Reference to an image file in Firebase Storage
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference("photos");
        StorageReference imagesRef = storageRef.child(mDataset.get(position).id);
        Log.d("ID DATA",mDataset.get(position).id);
        // ImageView in your Activity
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String userid = user.getUid();
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference eventDb = db.getReference("events/"+mDataset.get(position).id);
        Log.d("MUTABLEDATA","eventDb : "+"events/"+mDataset.get(position).id);


        holder.bind(mDataset.get(position),eventDb,userid);
        // Load the image using Glide
        Glide.with(mContext)
                .load(imagesRef)
                .into(holder.mImageView);
        holder.category_text.setText(mDataset.get(position).kategori);
        holder.tvw_total.setText(String.valueOf(mDataset.get(position).likeCount));
        if(mDataset.get(position).likes.containsKey(userid)){
            holder.tvw_like.setText("UNLIKE");
        } else {
            holder.tvw_like.setText("LIKE");

        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void changeData(ArrayList<Event> myDataset){
        this.mDataset = myDataset;
        notifyDataSetChanged();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "EventAdapter ViewHolder";
        // each data item is just a string in this case
        public TextView mTextView;
        public ImageView mImageView;
        public TextView category_text;
        public TextView tvw_total;
        public TextView tvw_like;
        private Event eventData;
        private DatabaseReference eventRef;
        private String userid;


        public ViewHolder(View v) {

            super(v);
            tvw_like = v.findViewById(R.id.tvw_like);
            tvw_total = v.findViewById(R.id.tvw_total);
            mTextView = v.findViewById(R.id.title_text);
            category_text = v.findViewById(R.id.category_text);
            mImageView = v.findViewById(R.id.imvw_event);


            tvw_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(tvw_like.getText().equals("LIKE")){
                        tvw_like.setText("UNLIKE");
                    } else {
                        tvw_like.setText("LIKE");
                    }

                    eventRef.runTransaction(new Transaction.Handler() {
                        @Override
                        public Transaction.Result doTransaction(MutableData mutableData) {
                            Event p = mutableData.getValue(Event.class);
                            if (p == null) {
                                return Transaction.success(mutableData);
                            }

                            if (p.likes.containsKey(userid)) {
                                // Unstar the post and remove self from stars
                                p.likeCount = p.likeCount - 1;
                                p.likes.remove(userid);
                            } else {
                                // Star the post and add self to stars
                                p.likeCount = p.likeCount + 1;
                                p.likes.put(userid, true);
                            }

                            // Set value and report transaction success
                            mutableData.setValue(p);
                            return Transaction.success(mutableData);
                        }

                        @Override
                        public void onComplete(DatabaseError databaseError, boolean b,
                                               DataSnapshot dataSnapshot) {
                            // Transaction completed
                            Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                        }
                    });
                }
            });
        }
        public void bind(Event eventData, DatabaseReference eventRef, String userid){
            this.eventRef = eventRef;
            this.eventData = eventData;
            this.userid = userid;
        }

    }
}

