package com.songostudio.sedayuku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.songostudio.sedayuku.main.MainActivity;

public class InformationFormActivity extends AppCompatActivity {

    private EditText edtxNama;
    private EditText edtxAlamat;
    private EditText edtxKTP;
    private Button btnSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_form);
        edtxNama = (EditText) findViewById(R.id.edtxNama);
        edtxAlamat = (EditText) findViewById(R.id.edtxAlamat);
        edtxKTP = (EditText) findViewById(R.id.edtxKTP);
        btnSimpan = (Button) findViewById(R.id.btnSimpan);
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference userData = database.getReference("users/"+user.getUid());
                userData.child("noktp").setValue(edtxKTP.getText().toString());
                userData.child("nama").setValue(edtxNama.getText().toString());
                userData.child("alamat").setValue(edtxAlamat.getText().toString());
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            }
        });
    }
}
