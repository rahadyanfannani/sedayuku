package com.songostudio.sedayuku.category;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.songostudio.sedayuku.EventFormActivity;
import com.songostudio.sedayuku.InformationFormActivity;
import com.songostudio.sedayuku.R;
import com.songostudio.sedayuku.main.EventAdapter;
import com.songostudio.sedayuku.main.MainActivity;
import com.songostudio.sedayuku.model.Category;
import com.songostudio.sedayuku.model.Event;

import java.util.ArrayList;
import java.util.Collections;

public class CategoryActivity extends AppCompatActivity {

    private static final String TAG = "CategoryActivity" ;
    GridView gdvw_categories;
    CategoryAdapter categoriesAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        gdvw_categories = (GridView) findViewById(R.id.gdvw_categories);
        final ArrayList<Category> categories= new ArrayList<Category>();
        Category cKebakaran = new Category("Kebakaran",-1);
        Category cKriminal = new Category("Kriminal",-1);
        Category cPencurian = new Category("Pencurian",-1);
        Category cLain = new Category("Lain lain",-1);
        categories.add(cKebakaran);
        categories.add(cKriminal);
        categories.add(cPencurian);
        categories.add(cLain);
        categoriesAdapter = new CategoryAdapter(getApplicationContext(),categories);
        gdvw_categories.setAdapter(categoriesAdapter);
        gdvw_categories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), EventFormActivity.class);
                intent.putExtra("category",categories.get(i).getTitle());
                startActivity(intent);
            }
        });






    }
}
