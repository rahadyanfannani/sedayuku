package com.songostudio.sedayuku.category;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.songostudio.sedayuku.EventFormActivity;
import com.songostudio.sedayuku.R;
import com.songostudio.sedayuku.model.Category;
import com.songostudio.sedayuku.model.Event;

import org.w3c.dom.Text;

import java.util.ArrayList;


/**
 * Created by rahad on 14/11/2017.
 */

public class CategoryAdapter extends BaseAdapter {

    private ArrayList<Category> mDataset;
    private Context mContext;


    // Provide a suitable constructor (depends on the kind of dataset)
    public CategoryAdapter(Context context ,ArrayList<Category> myDataset) {
        mDataset = myDataset;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int i) {
        return mDataset.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (view == null) {
            gridView = inflater.inflate(R.layout.category_view, null);

            TextView tvw_category = (TextView) gridView.findViewById(R.id.tvw_category);
            ImageView imvw_category = (ImageView) gridView.findViewById(R.id.imvw_category);
            tvw_category.setText(mDataset.get(i).getTitle());
            imvw_category.setImageResource(R.drawable.logo);

        } else {
            gridView = (View) view;
        }
        return gridView;
    }
}

